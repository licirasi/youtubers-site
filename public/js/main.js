var youtubers = [];
$(document).ready(function () {

    $.ajax({
        type: "GET",
        url: "https://youtubers-papillon.rhcloud.com/api/v1/wall/web",
        dataType: "jsonp",
        success: function (resp) {
            resp = resp;
            youtubers = resp;
            console.log(resp);
            for (var i = 0; i < resp.length; i++) {
                var pic = '<div class="pic"><img src="' + resp[i].thumbnails.medium.url + '" class="pic-image" alt="Pic" />';
                pic = pic + '<span class="pic-caption left-to-right" onClick="go(youtubers[' + i + '])"><p style="margin-bottom:50px;">Nomination: ' + resp[i].expectedToDecide + '</p><p style="font-size: 1.2rem;">' + resp[i].title + '</p></span></div>';
                $("#conteinerWall").append(pic);
            }
            var finalpic = '<div class="pic"><img src="img/photo.png" class="pic-image" alt="Pic" />';
            finalpic = finalpic + '<span class="pic-caption left-to-right"  onClick="vaigiu()"><p style="margin-bottom:50px;">Other...</p><p style="font-size: 1.2rem;">download the app</p></span></div>';
            $("#conteinerWall").append(finalpic);
        },
        error: function () {
            // in caso negativo inserisco un alert
            alert("sorry, error");
        }
    });

    //for use within settings
    var fadeSlides = false,
        coloursSet = false;

    $('.slide').css("opacity", 1);

    //initalize onepage scroll
    $(".main").onepage_scroll({
        sectionContainer: "section"
    });

    //animate in video so you dont see resize
    setTimeout(function () {
        $('#big-video-wrap').show().animate({
            opacity: 1
        });

    }, 2500);

    //text slider on homepage
    $(".demo1 .rotate").textrotator({
        animation: "fade",
        speed: 2000
    });

    //UI handle
    $('.take-a-look').on('click', function () {

        $('.onepage-pagination li:nth-child(3)').children().trigger('click');
    });

    /*==================================
    =            Navigation            =
    ==================================*/

    $('.onepage-pagination').addClass("animated fadeInRight delay-3");

    $('.onepage-pagination').children().each(function () {

        switch ($(this).children().data('index')) {

        case 1:
            $(this).children().append("<i class='entypo-home'></i>");
            break;
        case 2:
            $(this).children().append("<i class='entypo-dot'></i>");
            break;
        case 3:
            $(this).children().append("<i id='dot2' class='entypo-dot-2'></i>");
            break;
        case 4:
            $(this).children().append("<i id='dot3' class='entypo-dot-3'></i>");
            break;
        case 5:
            $(this).children().append("<i class='entypo-mail'></i>");
            break;
        }
    });
    /*-----  End of Navigation  ------*/

    /*======================================================
    =            Animate slide when scrolled to            =
    ======================================================*/
    //use .delay-
    // 1 equals 500ms
    // 2 equals 1000s
    // 3 equals 1500ms etc

    setInterval(function () {

        if ($("body").hasClass("viewing-page-2")) {

            $('.slide-1-title').removeClass('hide').addClass('animated fadeInRight delay-1');
            $('.slide-1-1').removeClass('hide').addClass('animated fadeInRight delay-3');
            $('.iphone-1').removeClass('hide').addClass('animated fadeInUp delay-3');
            $('.slide-1-2').removeClass('hide').addClass('animated fadeInDown delay-5');
            $('.slide-1-3').removeClass('hide').addClass('animated fadeInDown delay-6');
            $('.slide-1-scroll').removeClass('hide').addClass('animated fadeInDown delay-12');

            $('.icon span').css("color", "#fff");

            console.log(fadeSlides);
            if (fadeSlides == true) {

                $('.slide-1').css("background-color", "#1ABC9C");
            };
        }
        if ($("body").hasClass("viewing-page-3")) {

            $('.iphone-2').removeClass('hide').addClass('animated fadeInDown delay-1');
            $('.slide-2-title').removeClass('hide').addClass('animated fadeIn delay-3');
            $('.slide-2-1').removeClass('hide').addClass('animated fadeIn delay-4');
            $('.slide-2-2').removeClass('hide').addClass('animated fadeInDown delay-5');
            $('.slide-2-3').removeClass('hide').addClass('animated fadeInDown delay-6');
            $('.slide-2-4').removeClass('hide').addClass('animated fadeInDown delay-9');
            $('.slide-2-5').removeClass('hide').addClass('animated fadeInDown delay-10');
            $('.slide-2-6').removeClass('hide').addClass('animated fadeInDown delay-11');
            $('.slide-2-scroll').removeClass('hide').addClass('animated fadeInDown delay-15');

            $('.icon span').css("color", "#fff");

            if (fadeSlides == true) {

                $('.slide-2').css("background-color", "#34495e");
            }
        }
        if ($("body").hasClass("viewing-page-4")) {

            $('.slide-3-title').removeClass('hide').addClass('animated fadeInLeft delay-1');
            $('.slide-3-1').removeClass('hide').addClass('animated fadeInLeft delay-3');
            $('.iphone-3').removeClass('hide').addClass('animated fadeInUp delay-3');
            $('.slide-3-2').removeClass('hide').addClass('animated fadeInDown delay-5');
            $('.slide-3-3').removeClass('hide').addClass('animated fadeInDown delay-6');
            $('.slide-3-scroll').removeClass('hide').addClass('animated fadeInDown delay-12');

            $('.icon span').css("color", "#333");

            if (fadeSlides === true) {

                $('.slide-3').css("background", "#f1c40f");
            }
        }
        if ($("body").hasClass("viewing-page-5")) {
            $('.icon span').css("color", "#fff");
        }
        if (fadeSlides == true && coloursSet == false) {

            $('.slide-1').css("background", "#e74c3c");
            $('.slide-2').css("background", "#1ABC9C");
            $('.slide-3').css("background", "#34495e");
            coloursSet = true;
        }

    }, 300);

    /*-----  End of Animate slide when scrolled to  ------*/
});

function go(youtuber) {
    window.open('https://www.youtube.com/channel/' + youtuber.channelId + '');
}

function vaigiu() {
    $("#dot3").click();
}